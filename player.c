#include <math.h>
#include <stdbool.h>
#include "game.h"
#include "game_config.h"
#include "movement.h"
#include "player.h"

void setup_player( Game * game ) {
	static char bitmap[] =
		"o"
		"+"
		"^";
	sprite_id firstSaveBlock = game->blocks[0];
	int spriteWidth = 1,
		spriteHeight = 3,
		startLeft = sprite_x(firstSaveBlock) + (sprite_width(firstSaveBlock) / 2),
		startTop = round(sprite_y(game->blocks[0])) - sprite_height(game->player) - 2;
	

	game->player = sprite_create(startLeft, startTop, spriteWidth, spriteHeight, bitmap);
	reset_player(game);
}

void move_player( Game * game, int key, double jump_time ) {
	switch (game->level) {
		case 1:
			move_player_lv1(game, key, jump_time);
			break;
		case 2:
			move_player_lv2(game, key, jump_time);
			break;
		case 3:
			move_player_lv3(game, key, jump_time);
			break;
	}
}

void move_player_lv1( Game * game, int key, double jump_time ) {
	switch (key) {
		case 4:
			if ((sprite_x(game->player) > 0) && (game->playerLanded >= 0)) {
				sprite_move_left(game->player);
				update_player(game, jump_time);
			}
			break;
		case 5:
			if ((sprite_x(game->player) < screen_width()-1) && (game->playerLanded >= 0)) {
				sprite_move_right(game->player);
				update_player(game, jump_time);
			}
			break;
	}
}

void move_player_lv2( Game * game, int key, double jump_time ) {
	switch (key) {
		case 4: // left arrow
			if ((sprite_x(game->player) > 0) && (game->playerLanded >= 0)) {
				sprite_towards_left(game->player);

				update_player(game, jump_time);
			}
			break;
		case 5: //right arrow
			if ((sprite_x(game->player) < screen_width()-1) && (game->playerLanded >= 0)) {
				sprite_towards_right(game->player);
				update_player(game, jump_time);
			}
			break;
		case 3:	// up arrow
			if (game->playerLanded >= 0) {
				sprite_towards_up(game->player);
				update_player(game, jump_time);
			}
			break;
		case 2: // down arrow
			if (game->playerLanded >= 0) {
				sprite_stop(game->player);
				update_player(game, jump_time);
			}
			break;
	}
}

void move_player_lv3( Game * game, int key, double jump_time ) {
	switch (key) {
		case 4: // left arrow
			if ((sprite_x(game->player) > 0) && (game->playerLanded >= 0)) {
				sprite_towards_left(game->player);

				update_player(game, jump_time);
			}
			break;
		case 5: //right arrow
			if ((sprite_x(game->player) < screen_width()-1) && (game->playerLanded >= 0)) {
				sprite_towards_right(game->player);
				update_player(game, jump_time);
			}
			break;
		case 3:	// up arrow
			if (game->playerLanded >= 0) {
				sprite_towards_up(game->player);
				update_player(game, jump_time);
			}
			break;
		case 2: // down arrow
			if (game->playerLanded >= 0) {
				sprite_stop(game->player);
				update_player(game, jump_time);
			}
			break;
	}
}


void update_player( Game * game, double jump_time ) {
	int scanBlock = 0;
	if (  // recheck if it still land on the same block
			(game->playerLanded >= 0)
			&&
			isLandOnBlock(game->player, game->blocks[game->playerLanded])
		) { // already landed
			// move with the landed block
		sprite_move_to(game->player, sprite_x(game->player), round(sprite_y(game->blocks[game->playerLanded])) - sprite_height(game->player));
		if (sprite_dy(game->player)>0) { // tuen off dy if landed
			sprite_turn_to(game->player, sprite_dx(game->player), 0);
		}
	} else {
		// above test not pass, player not landed
		game->playerLanded = -1;
		while (scanBlock < NUM_OF_BLOCKS) { // search if her is now landed?
			if (isLandOnBlock(game->player, game->blocks[scanBlock])) {
				// if landed, take record
				game->playerLanded = scanBlock;
				// attache the player to the landed block
				sprite_move_to(game->player, sprite_x(game->player), round(sprite_y(game->blocks[scanBlock])) - sprite_height(game->player));
				// if it is a new landing, get a score
				if (game->safeblock[game->playerLanded]) {
					++game->score;
				}
				// turn off dy
				if (sprite_dy(game->player)>0) {
						sprite_turn_to(game->player,sprite_dx(game->player),0);
				}
				break;
			}
			++scanBlock;
		}
		if (game->playerLanded == -1) { // if still not landed, reset dy
			sprite_turn_to(game->player, sprite_dx(game->player), 2);
			//fixed_player_turn(game->player, 0);
		}
	}

	// handle out of screen issue, turn off dx if out of screen.
	if (
			(game->level > 1)
			&& (
					(sprite_x(game->player) <= 0)
					|| (sprite_x(game->player) >= screen_width() - 1)
				)
	   ) {
		sprite_stop(game->player);
	}
	if (sprite_dy(game->player)<0) { //player is jumping
		fixed_player_turn(game->player, 0);
	} else {
		sprite_step(game->player);
	}
}

bool isLandOnBlock(sprite_id player, sprite_id block) {
	return (
				(round(sprite_x(player)) >= round(sprite_x(block)))
				&&
				(round(sprite_x(player)) < round(sprite_x(block)) + sprite_width(block))
				&&
				(
					(round(sprite_y(player)) >= round(sprite_y(block)) - sprite_height(player))
					&&
					(round(sprite_y(player)) < round(sprite_y(block)) + sprite_height(block))
				)
			);
}

void show_player( Game * game ) {
	if (sprite_x(game->player) <= 0) {
		sprite_move_to(game->player, 0, sprite_y(game->player));
		sprite_turn_to(game->player, 0, sprite_dy(game->player));
	} else if (sprite_x(game->player) >= screen_width() - sprite_width(game->player)) {
		sprite_move_to(game->player, screen_width() - sprite_width(game->player), sprite_y(game->player));
		sprite_turn_to(game->player, screen_width() - sprite_width(game->player), sprite_dy(game->player));
	}
	sprite_draw(game->player);
}


void reset_player( Game * game ) {
	int move_on_each_timestamp_x = 0,
		move_on_each_timestamp_y = PLAYER_MOVE_STEP;
	sprite_id firstSaveBlock = game->blocks[0];
	sprite_turn_to(game->player, move_on_each_timestamp_x, move_on_each_timestamp_y);
	sprite_move_to(game->player, sprite_x(firstSaveBlock) + (sprite_width(firstSaveBlock)/2), round(sprite_y(firstSaveBlock)) - sprite_height(game->player));
	game->playerLanded = 0;
}

bool isPlayerDie( Game * game ) {
	return (
			(round(sprite_y(game->player)) < 3)
			||
		   	(round(sprite_y(game->player)) + sprite_height(game->player) > screen_height() - 4)
		   	||
		   	(
		   		(game->playerLanded >= 0)
		   		&&
		   		(!game->safeblock[game->playerLanded])
		   	)
		   );
}