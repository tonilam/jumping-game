#ifndef MOVEMENT_H_
#define MOVEMENT_H_

void sprite_move_left( sprite_id player );
void sprite_move_right( sprite_id player );

void sprite_towards_left( sprite_id player );
void sprite_towards_right( sprite_id player );
void sprite_towards_up( sprite_id player );
void sprite_stop( sprite_id player );

void fixed_player_turn( sprite_id sprite, double degrees );
void gravity_acceleration( sprite_id player, double timeElapsed );

#endif