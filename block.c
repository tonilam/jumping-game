#include <stdlib.h>
#include <time.h>
#include <math.h>
#include "game.h"
#include "game_config.h"
#include "movement.h"
#include "block.h"

void setup_blocks( Game * game ) {
	static char bitmapSafeBlock[22] =
		"========"
		"========";
	static char bitmapForbiddenBlock[22] =
		"xxxxxxxx"
		"xxxxxxxx";
	static char bitmapSafeBlockArray[8][22] =
		{
			"==="
		 	"===",
			"===="
		 	"====",
			"====="
		 	"=====",
			"======"
		 	"======",
			"======="
		 	"=======",
			"========"
		 	"========",
			"========="
		 	"=========",
			"=========="
		 	"==========",
		};
	static char bitmapForbiddenBlockArray[8][22] =
		{
			"xxx"
		 	"xxx",
			"xxxx"
		 	"xxxx",
			"xxxxx"
		 	"xxxxx",
			"xxxxxx"
		 	"xxxxxx",
			"xxxxxxx"
		 	"xxxxxxx",
			"xxxxxxxx"
		 	"xxxxxxxx",
			"xxxxxxxxx"
		 	"xxxxxxxxx",
			"xxxxxxxxxx"
		 	"xxxxxxxxxx",
		};
	srand( time( NULL ) );

	bool init_safeblocks = false;
	// randomly assign safe blocks and forbidden blocks
	while (!init_safeblocks) {
		game->safeblock[0] = true;  // first one must be save
		int saveboxCounter = 1;
		for (int i = 1; i < NUM_OF_BLOCKS; ++i) {
			if (rand() % 2 == 0) {
				game->safeblock[i] = true;
				++saveboxCounter;
			} else {
				game->safeblock[i] = false;
			}
		}
		init_safeblocks = saveboxCounter == NUM_OF_SAFEBOX;
	}

	int move_on_each_timestamp_x = 0,
		move_on_each_timestamp_y = BLOCK_MOVE_STEP;
	int spriteWidth = 8,
		spriteHeight = 2,
		scrollbarHeight = 3,
		startLeft = rand() % (screen_width() - 8),
		startTop = screen_height() - scrollbarHeight - spriteHeight;
	int variation = 0;

	for (int i = 0; i < NUM_OF_BLOCKS; ++i) {
		if (i > 0) {
			startTop += (rand() % 4) + 1;
		}
		if (game->safeblock[i]){
			switch (game->level) {
				case 1:
					game->blocks[i] = sprite_create(startLeft, startTop+i, spriteWidth, spriteHeight, bitmapSafeBlock);
					break;
				case 2:
					variation = rand() % 8;
					spriteWidth = variation + 3;
					game->blocks[i] = sprite_create(startLeft, startTop+i, spriteWidth, spriteHeight, bitmapSafeBlockArray[variation]);
					break;
				case 3:
					variation = rand() % 8;
					spriteWidth = variation + 3;
					game->blocks[i] = sprite_create(startLeft, startTop+i, spriteWidth, spriteHeight, bitmapSafeBlockArray[variation]);
					break;
			}
		} else {
			switch (game->level) {
				case 1:
					game->blocks[i] = sprite_create(startLeft, startTop+i, spriteWidth, spriteHeight, bitmapForbiddenBlock);
					break;
				case 2:
					variation = rand() % 8;
					spriteWidth = variation + 3;
					game->blocks[i] = sprite_create(startLeft, startTop+i, spriteWidth, spriteHeight, bitmapForbiddenBlockArray[variation]);
					break;
				case 3:
					variation = rand() % 8;
					spriteWidth = variation + 3;
					game->blocks[i] = sprite_create(startLeft, startTop+i, spriteWidth, spriteHeight, bitmapForbiddenBlockArray[variation]);
					break;
			}
		}
		sprite_turn_to(game->blocks[i], move_on_each_timestamp_x, move_on_each_timestamp_y);
	}

	// search if any block too close, redefine position
	for (int incumbent = 1; incumbent < NUM_OF_BLOCKS; ++incumbent) {
		reset_block(game, incumbent);
	}
}

void reset_block( Game * game, int incumbent ) {
	//wait_char();
	int startLeft, startTop;
	int neighborhood = incumbent - 1;
	if (neighborhood < 0) {
		neighborhood = NUM_OF_BLOCKS - 1;
	}
	while (!isSafePosition(game->blocks, incumbent)) {
		startLeft = rand() % (screen_width() - 8);
		if (sprite_y(game->blocks[neighborhood]) <= screen_height() - 3) { // if neighborhood is entered the screen, 
			startTop = screen_height() - 1; // make sure the incumbent top is at least on the bottom
		} else { // else, set incumbent below its neighborhood
			startTop = (int) sprite_y(game->blocks[neighborhood]) + sprite_height((game->blocks[neighborhood]));
			startTop += 1; // make sure the incumbent is below its neighborhood
		}
		startTop += rand() % 10; // give some variation on the space between incumbent and its neighborhood
		sprite_move_to(game->blocks[incumbent], startLeft, startTop);
	}
}


void update_blocks( Game * game ) {
	for (int incumbent = 0; incumbent < NUM_OF_BLOCKS; ++incumbent) {
		if (sprite_y(game->blocks[incumbent]) > 3){
			sprite_step(game->blocks[incumbent]);
			if ((sprite_x(game->blocks[incumbent]) < sprite_x(game->player))
				&& (sprite_x(game->blocks[incumbent]) + sprite_width(game->blocks[incumbent]) > sprite_x(game->player))
				) {
				if ((sprite_y(game->blocks[incumbent]) > sprite_y(game->player))
					&& (sprite_y(game->blocks[incumbent]) < sprite_y(game->player) + sprite_height(game->player))) {
					game->playerLanded = incumbent;
					if (!game->safeblock[incumbent]) {
						game->over = true;
					}
				}
			}
		} else {
			reset_block(game, incumbent);
		}
	}
	if (game->playerLanded >= 0) {
		sprite_move_to(game->player, sprite_x(game->player), round(sprite_y(game->blocks[game->playerLanded])) - sprite_height(game->player));
	}
}

void show_blocks( sprite_id *block ) {
	for (int i = 0; i < NUM_OF_BLOCKS; ++i) {
		sprite_draw(block[i]);
	}
}

void change_block_speed( Game * game, int key ) {
	double speed;
	switch (key) {
		case '1':
			speed = BLOCK_MOVE_STEP/4.0;
			break;
		case '2':
			speed = BLOCK_MOVE_STEP;
			break;
		case '3':
			speed = BLOCK_MOVE_STEP*4;
			break;
	}
	for (int i = 0; i < NUM_OF_BLOCKS; ++i) {
		sprite_turn_to(game->blocks[i], 0, speed);
	}
}

bool isSafePosition(sprite_id * blocks, int target) {
	if (sprite_y(blocks[target]) < screen_height() - sprite_height(blocks[target])) {
		return false;
	}

	for (int incumbent = 0; incumbent < NUM_OF_BLOCKS; ++incumbent) {
		if (incumbent != target) {
			if (
					(	// conflict on Y-axis
						(sprite_y(blocks[incumbent]) + sprite_height(blocks[incumbent]) > sprite_y(blocks[target]) - 3)
						&&
						(sprite_y(blocks[incumbent]) < sprite_y(blocks[target]) + sprite_height(blocks[incumbent]) + 3)
					)
					&&
					(	// conflict on X-axis
						(sprite_x(blocks[incumbent]) + sprite_width(blocks[incumbent]) > sprite_x(blocks[target]) - 4)
						&&
						(sprite_x(blocks[incumbent]) < sprite_x(blocks[target]) + sprite_width(blocks[incumbent]) + 4)
					)
				) {
				// incumbent is inside the safety horizon of target
				return false;
			}
		}
	}
	return true;
}