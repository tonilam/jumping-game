#ifndef PLAYER_H_
#define PLAYER_H_

#include <stdbool.h>
#include "cab202_sprites.h"
#include "cab202_graphics.h"
#include "game.h"

void setup_player( Game * game );
void move_player( Game * game, int key, double jump_time );
void update_player( Game * game, double jump_time );
void move_player_lv1( Game * game, int key, double jump_time );
void move_player_lv2( Game * game, int key, double jump_time );
void move_player_lv3( Game * game, int key, double jump_time );
void show_player( Game * game );
void reset_player( Game * game );
bool isLandOnBlock(sprite_id player, sprite_id block);
bool isPlayerDie( Game * game);

#endif
