#ifndef STATUS_H_
#define STATUS_H_

#include "game.h"

void setup_status( Game * game );
void update_status( Game * game, char *speedstatus);

#endif