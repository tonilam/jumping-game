#ifndef GAME_H_
#define GAME_H_

#include <stdbool.h>
#include "cab202_sprites.h"
#include "cab202_timers.h"
#include "game_config.h"

typedef struct Game {                  
	bool over;
	int live;
	int level;
	int score;
	double start_time;

	sprite_id player;
	int playerLanded;	
	sprite_id blocks[NUM_OF_BLOCKS];	
	bool safeblock[NUM_OF_BLOCKS];	

	timer_id timestamp;
	timer_id playertimestamp;
} Game;								  

#endif