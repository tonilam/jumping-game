#include <stdio.h>
#include <math.h>
#include <string.h>
#include "cab202_graphics.h"
#include "game.h"

void setup_status( Game * game ) {
	game->live = MAX_LIVES;
	game->over = false;
	game->level = 3;
	game->score = 0;
	game->start_time = get_current_time();
	game->timestamp = create_timer(GAME_TIMESTAMP_TIMEOUT);
	game->playertimestamp = create_timer(PLAYER_TIMESTAMP_TIMEOUT);
}

void update_status( Game * game, char *speedstatus ) {
	char status[screen_width()];
	char statusborder[screen_width()];
	char timestatus[21];
	char scorebar[screen_width()];
	int i;

	int sec = (int) (get_current_time() - game->start_time) % 60;
	int min = (int) (get_current_time() - game->start_time) / 60;
	sprintf(status, "| Lives remaining: %d", game->live);

	// determine the time digit and display in correct spacing
	if ((min >= 10) && (sec >= 10)) {
		sprintf(timestatus, "Playing Time: %2d:%2d |", min, sec);
	} else if ((min >= 10) && (sec < 10)) {
		sprintf(timestatus, "Playing Time: %2d:0%d |", min, sec);
	} else if ((min < 10) && (sec >= 10)) {
		sprintf(timestatus, "Playing Time: 0%d:%2d |", min, sec);
	} else if ((min < 10) && (sec < 10)) {
		sprintf(timestatus, "Playing Time: 0%d:0%d |", min, sec);
	}

	// generate the border for upper bound and lower bound
	statusborder[0] = '+';
	for (i = 1; i < screen_width()-1; i++){
		statusborder[i] = '-';
	}
	statusborder[screen_width()-1] = '+';

	// clear statusbar
	for (i = 0; i < screen_width()-1; ++i) {
		draw_char(i,2, ' ');
	}
	// clear scorebar
	for (i = 0; i < screen_width()-1; ++i) {
		draw_char(i,screen_height()-2, ' ');
	}
	// display result
	draw_string(0, 0, statusborder);
	draw_string(3, screen_height()-2, scorebar);
	draw_string(0, 1, status);
	draw_string(screen_width()-21, 1, timestatus);
	draw_string(0, 2, statusborder);

	sprintf(scorebar, "Level %d, Score: %d", game->level, game->score);
	draw_string(0, screen_height()-3, statusborder);
	draw_char(0,screen_height()-2, '|');
	draw_char(screen_width()-1, screen_height()-2, '|');
	draw_string(2, screen_height()-2, scorebar);
	draw_string(0, screen_height()-1, statusborder);

	if (game->level >= 3) {
		draw_string(screen_width()- strlen(speedstatus) -2, screen_height()-2, speedstatus);
	}
}
