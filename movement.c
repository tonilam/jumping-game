#include <math.h>
#include "cab202_sprites.h"
#include "cab202_graphics.h"
#include "movement.h"

static int jump = -10;

void sprite_move_left( sprite_id player ) {
	int reset_dy = sprite_dy(player);
	sprite_turn_to(player, -1, 0);
	sprite_step(player);
	sprite_turn_to(player, 0, reset_dy);
}

void sprite_move_right( sprite_id player ) {
	int reset_dy = sprite_dy(player);
	sprite_turn_to(player, 1, 0);
	sprite_step(player);
	sprite_turn_to(player, 0, reset_dy);
}

void sprite_towards_left( sprite_id player ) {
	sprite_turn_to(player, -1, sprite_dy(player));
}

void sprite_towards_right( sprite_id player ) {
	sprite_turn_to(player, 1, sprite_dy(player));
}

void sprite_towards_up( sprite_id player ) {
	jump = 0;
	fixed_player_turn(player, 0);
}

void sprite_stop( sprite_id player ) {
	sprite_turn_to(player, 0, sprite_dy(player));
}

void fixed_player_turn( sprite_id sprite, double degrees ) {
	int dy = -8;
	int dx = sprite_dx(sprite) - jump * jump;
	if (sprite_dx(sprite)<0) {
		dx = sprite_dx(sprite) + jump * jump;
	}
	if (jump == 0){
		dy = -8;
	} else {
		dy += (jump/2) * (jump/2);
	}
	if (jump >= 0 ) {
		double old_dy = sprite_dy(sprite) ;
		sprite_turn_to(sprite, dx, dy);
		sprite_step(sprite);
		sprite_turn_to(sprite, dx, old_dy+1);
	}
	jump++;
	// double radians = degrees * M_PI / 180;
	// double s = sin( radians );
	// double c = cos( radians );
	// double dx = c * sprite->dx + s * sprite->dy;
	// double dy = -s * sprite->dx + c * sprite->dy;
	// sprite->dx = dx;
	// sprite->dy = -dy;
}

void gravity_acceleration( sprite_id player, double timeElapsed ) {
	player->x += player->dx * timeElapsed;
	player->y += player->dy * timeElapsed;
	player->dx += 0; // No horizontal acceleration
	player->dy += 9.8 * timeElapsed;
}