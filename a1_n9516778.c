#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>
#include <stdbool.h>
#include "cab202_graphics.h"
#include "cab202_sprites.h"
#include "cab202_timers.h"

#include "game.h"
#include "game_config.h"
#include "status.h"
#include "player.h"
#include "block.h"
#include "movement.h"

/* Fucntions declaration */
void setup_game( Game * game );
void start_game( Game * game );
bool continue_game();

int main( void ) {
	Game game;
	auto_save_screen = true;
	setup_screen();
	do {
		setup_game(&game);
		start_game(&game);
	} while (continue_game());
	cleanup_screen();
	return 0;
}

void setup_game( Game * game ) {
	setup_status(game);
	setup_blocks(game);
	setup_player(game);
}

void start_game( Game * game ) {
	int user_control = 50;
	static char *speedStatus[3] = {"SLOW", "NORM", "FAST"};
	static char speedstatus[16];
	sprintf(speedstatus, "%s", "NORM");
	int user_control_prev = 50;
	double jump_time = get_current_time();
	double transittimer = 0;
	char nextaction = 0;
	show_player(game);
	show_blocks(game->blocks);
	update_status(game, speedstatus);
	while (!game->over || (game->live > 0)) {
		game->over = false;
		if (timer_expired(game->timestamp)){
			clear_screen();
			nextaction = get_char();

			// This loop is used to make sure the last input is being process when there is multiple input before
			// the timer expired
			do{
				user_control = nextaction;
				nextaction = get_char();
			} while (!(nextaction == EOF));

			if ((user_control > 0) && (user_control < 10)){
				if (game->playerLanded == -1) {
					jump_time = get_current_time();
				}
				move_player(game, user_control, jump_time);
			} else if ((game->level >= 3) && (user_control >= 49) && (user_control <= 51)) {
				if (user_control != user_control_prev) {
					transittimer = get_current_time();
					sprintf(speedstatus, "%s%s%s", speedStatus[user_control_prev-49], " -> ", speedStatus[user_control-49]);
					user_control_prev = user_control;
					change_block_speed(game, user_control);
				}
			} else if (user_control == 'l') {
				int currentLevel = game->level;
				setup_status(game);
				if (currentLevel == 3) {
					currentLevel = 1;
				} else {
					++currentLevel;
				}
				game->level = currentLevel;
				setup_blocks(game);
				setup_player(game);
			}
			if ((get_current_time() - transittimer > 2) && (get_current_time() - transittimer < 5)) {
				sprintf(speedstatus, "%s", speedStatus[user_control_prev-49]);
			}

			if (timer_expired(game->playertimestamp)){
				update_blocks(game);
				update_player(game, jump_time);
			}

			show_player(game);
			show_blocks(game->blocks);
			update_status(game, speedstatus);
			show_screen();
			game->over = isPlayerDie(game);
		}
		if (game->over) {
			--game->live;
			sprintf(speedstatus, "%s", "NORM");
			user_control_prev = 50;
			setup_blocks(game);
			setup_player(game);
		}
	}
	clear_screen();
	update_status(game, speedstatus);
	show_screen();
}

bool continue_game() {
	char option;

	draw_string(screen_width()/2 - 10, screen_height()/2, "Do you want to continue?");
	draw_string(screen_width()/2 - 10, screen_height()/2 + 1, "[r] continue");
	draw_string(screen_width()/2 - 10, screen_height()/2 + 2, "[q] quit");
	show_screen();
	do {
		option = wait_char();

		if (option == 'q') {
			return false;
		} else if (option == 'r') {
			return true;
		}
	} while ((option != 'q') && (option != 'r'));
	return false;
}
