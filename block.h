#ifndef BLOCK_H_
#define BLOCK_H_

#include "cab202_sprites.h"
#include "cab202_graphics.h"
#include "game.h"

void setup_blocks( Game * game );
void move_block( Game * game, int key );
void update_blocks( Game * game );
void reset_block( Game * game, int incumbent  );
void show_blocks( sprite_id *block );
void change_block_speed( Game * game, int key );
bool isSafePosition(sprite_id * blocks, int incumbent);

#endif
